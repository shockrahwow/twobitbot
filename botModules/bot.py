from discord.ext import commands
import botCFG
import textCommands
# ENABLED EXTENSIONS FOUND IN THIS LIST 
STARTUP_EXT = ['textCommands']
# Main Object for the bot to run off of which happens to be used else where
bot = commands.Bot(command_prefix=botCFG.prefix,description=botCFG.desc)

# this may be of some use: https://github.com/Rapptz/discord.py/blob/async/examples/reply.py 
# NOTE NOTE NOTE NOTE NOTE NOTE NOTE NOTE 
# help command redefined in textCommands.py
bot.remove_command('help') 

# starting up
@bot.event
async def on_ready():
    print("Client Logged In")
    print("username: " + bot.user.name)
    print("id: " + bot.user.id)

# these two will require some level of admin security as well at some point
@bot.command(pass_context=True)
async def load(ctx, ext_name : str):
    if not ctx.message.author.server_permissions.administrator:
        return 
    try:
        bot.load_extension(ext_name)
    except (AttributeError, ImportError) as e: # wow
        await bot.say("```py\n{}: {}\n```".format(type(e).__name__, str(e)))
        return 
    await bot.say("{} loaded!".format(ext_name))
    
    
@bot.command(pass_context=True)
async def unload(ctx, ext_name : str):
    if not ctx.message.author.server_permissions.administrator:
        return 
    bot.unload_extension(ext_name)
    await bot.say("{} unloaded.".format(ext_name))
   
if __name__ == "__main__":
    for extension in STARTUP_EXT:
        try:
            bot.load_extension(extension)
        except Exception as e:
            exc = "{} {}".format(type(e).__name__,e)
            print("Failed to load extension {}\n{}".format(extension,exc))
            
    bot.run(botCFG.token)